import composite.ProjectTask;
import composite.SoftwareProject;

import java.time.LocalDate;

/**
 * Composite pattern demo.
 * Implements a nested, recursive structure for SoftwareProjects that can consist of
 *  ProjectTasks and sub-SoftwareProjects (which can have subProjects and tasks themselves)
 */
public class DemoComposite {
    public static void main(String[] args) {
        ProjectTask task1 = new ProjectTask("Mark", "Write JUnit tests ", LocalDate.of(2020, 10, 10), 16);
        ProjectTask task2 = new ProjectTask("Linda", "Webservices", LocalDate.of(2020, 10, 5), 4);
        ProjectTask task3 = new ProjectTask("Freddy", "Website frontend", LocalDate.of(2020, 10, 15), 64);

        SoftwareProject subProject = new SoftwareProject("Web application City of Antwerp");
        subProject.add(task1);
        subProject.add(task2);
        subProject.add(task3);

        SoftwareProject masterProject = new SoftwareProject("Project City of Antwerp");
        masterProject.add(subProject);
        masterProject.add(new ProjectTask("Nancy", "Prepare project proposal", LocalDate.of(2020, 11, 5), 4));

        System.out.println(masterProject);
        System.out.println("Total effort: " + masterProject.getTime() + " hours");
    }
}

/*
OUTPUT:
Project City of Antwerp:
[Web application City of Antwerp:
[Write JUnit tests  (Mark) --> 16 hrs, Webservices (Linda) --> 4 hrs, Website frontend (Freddy) --> 64 hrs], Prepare project proposal (Nancy) --> 4 hrs]
Total effort: 88 hours
 */
