package publisher;

public interface Subscriber {

    void update(String action, Object object);
}
