package domain;

import publisher.Subscriber;

import java.util.Observable;
import java.util.Observer;

public class PuntSubscriber implements Subscriber {


    @Override
    public void update(String action, Object object) {
        System.out.println("Actie: " + action + " Object: " + object);

    }
}