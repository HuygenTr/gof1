package demo.pos.domain.sale;

import demo.pos.domain.product.ProductDescription;
import demo.pos.domain.sale.discount.Discount;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by overvelj on 14/11/2016.
 */
public class Sale {
	private List<SalesLineItem> slis;
	private boolean isComplete;
	private Payment payment;
	private long id;
	private List<Discount> promos;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Sale() {
		this.slis = new ArrayList<>();
		this.isComplete = false;
		this.promos = new ArrayList<>();
	}

	public void addDiscount(Discount promo){
		promos.add(promo);
	}

	public List<SalesLineItem> getSalesLineItems(){
		return slis;
	}

	public void makeSalesLineItem(ProductDescription itemDesc, int qty) {
		slis.add(new SalesLineItem(itemDesc,qty));
	}

	public Payment getPayment() {
		return payment;
	}

	public boolean isComplete() {
		return isComplete;
	}

	public void setComplete(){
		isComplete = true;
	}

	public double getTotal() {
		double base = getBaseTotal();
		return base + getDiscount(base);
	}

	private double getBaseTotal() {
		double sum=0;
		for(SalesLineItem sli : slis){
			sum += sli.getSubTotal();
		}
		return sum;
	}

	private double getDiscount(double base) {
		double	discount = 0.0;
		double total = base;
		for (Discount promo:promos){
			double singleDiscount=promo.getDiscount(total);
			total+=singleDiscount;
			discount += singleDiscount;
		}
		return discount;
	}


	public void makePayment(double i) {
		payment = new Payment(i);
	}

	public double getSalesLineItemTotal(ProductDescription product, int count){
		SalesLineItem sli = new SalesLineItem(product, count);
		return sli.getSubTotal();
	}
}
