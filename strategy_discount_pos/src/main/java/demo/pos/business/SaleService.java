package demo.pos.business;

import demo.pos.domain.product.ProductDescription;
import demo.pos.domain.sale.Sale;
import demo.pos.persistence.*;


/**
 * Created by overvelj on 14/11/2016.
 */
public class SaleService {
    private final SaleRepository salesRepository = Repositories.getRepositories().getSaleRepository();
    private ProductCatalog catalog = Repositories.getRepositories().getProductCatalog();



    public Sale addSale(){
        Sale s = new Sale();
        return salesRepository.insert(new Sale());
    }

    public long countSales(){
        return salesRepository.count();
    }

    public void addSalesLine(long saleId, long id, int qty) {
        ProductDescription itemDesc = catalog.getById(id);
        Sale s = getSale(saleId);
        s.makeSalesLineItem(itemDesc, qty);
        salesRepository.update(s);
    }
    public void endSale(long id) {
        Sale s = getSale(id);
        s.setComplete();
        salesRepository.update(s);
    }

    public Sale getSale(long id) {
        return salesRepository.getById(id);
    }

    public void makePayment(long id, double i) {
        Sale s = getSale(id);
        s.makePayment(i);
        salesRepository.update(s);
    }
}
