package demo.pos.persistence;

import demo.pos.domain.product.ProductDescription;

/**
 * @author Jan de Rijke.
 * dummy DB implementation class
 */
public class ProductDescriptionDbRepository implements ProductCatalog {

	 ProductDescriptionDbRepository() {
	}

	@Override
	public boolean update(ProductDescription value) {
		return false;
	}

	@Override
	public ProductDescription insert(ProductDescription value) {
		return null;
	}

	@Override
	public long count() {
		return 0;
	}

	@Override
	public ProductDescription getById(Long key) {
		return null;
	}
}