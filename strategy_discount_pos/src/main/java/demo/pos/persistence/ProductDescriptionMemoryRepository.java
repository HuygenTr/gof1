package demo.pos.persistence;

import demo.pos.domain.product.ProductDescription;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductDescriptionMemoryRepository implements ProductCatalog{
	// this is a repository for which ID's are in the objects that are added
	private Map<Long, ProductDescription> products=new HashMap<>();

	 ProductDescriptionMemoryRepository() {
	}

	@Override
	public ProductDescription insert(ProductDescription value) {
		products.put(value.getProductId(),value);
	 	return value;
	}

	public boolean update(ProductDescription pd) {
        return products.put(pd.getProductId(), pd)!=null;
    }

    public ProductDescription getById(Long id) {
        return products.get(id);
    }

	@Override
	public long count() {
		return products.size();
	}
}
