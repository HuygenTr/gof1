package demo.pos.persistence;

import demo.infra.Repository;
import demo.pos.domain.product.ProductDescription;

/**
 * @author Jan de Rijke.
 */
public interface ProductCatalog extends Repository<Long,ProductDescription> {
}
